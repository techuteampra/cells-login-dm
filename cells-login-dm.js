class CellsLoginDm extends Polymer.Element {

  static get is() {
    return 'cells-login-dm';
  }

  static get properties() {
    return {
      body: Object
    };
  }

  _triggerLogin(evt) {
    console.log("_triggerLogin", evt);

    this.body = this.formatBody(evt);
    console.log('this.body', this.body);
    this.$.loginDP.generateRequest();
  }

  formatBody(event) {
    console.log('formatBody', event);
    let user = { "correo": event.userId, "password": event.password }
    return user;
  }


  _handleLoginSuccess(evt) {
    console.log("loginSuccess", evt);
    localStorage.setItem('fastlunch-userLogged', JSON.stringify(evt.detail.usuario));
    localStorage.setItem('fastlunch-userToken', evt.detail.token);
    this.dispatchEvent(new CustomEvent('users-login-success', {
      bubbles: true,
      composed: true,
      detail: { 'user': evt.detail.user }
    }));
    this.dispatchEvent(new CustomEvent('users-login-success', {
      bubbles: true,
      composed: true,
      detail: { 'user': evt.detail.user }
    }));

    /*this.dispatchEvent( new CustomEvent('name-client-evt', {
      bubbles: true,
      composed: true,
      detail: evt.detail.backendUserResponse.clientId
    }));*/
  }
}

customElements.define(CellsLoginDm.is, CellsLoginDm);
